<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Funcionario extends CI_Controller
{
    public function pag_login()
    {
        $this->load->view('backend/template/html-header');
        $this->load->view('backend/loginfunc');
        $this->load->view('backend/template/html-footer');
    }

    public function login()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('txt-nome', 'Usuário', 'required|min_length[3]');
        $this->form_validation->set_rules('txt-rg', 'Senha', 'required|max_length[9]');

        if ($this->form_validation->run() == FALSE) {
            $this->pag_login();
        } else {
            $nome = $this->input->post('txt-nome');
            $rg = $this->input->post('txt-rg');
            $this->db->where('nome', $nome);
            $this->db->where('rg', $rg);
            $funclogado = $this->db->get('funcionario')->result();

            if (count($funclogado) == 1) {
                $dadosSessao['funclogado'] = $funclogado[0];
                $dadosSessao['logado'] = TRUE;
                $this->session->set_userdata($dadosSessao);
                redirect(base_url('index.php/funcionarios/plantao/lista_plantao'));
            } else {
                $dadosSessao['funclogado'] = NULL;
                $dadosSessao['logado'] = FALSE;
                $this->session->set_userdata($dadosSessao);
                redirect(base_url('index.php/funcionarios/login'));
            }
        }
    }

    public function logout()
    {
        $dadosSessao['funclogado'] = NULL;
        $dadosSessao['logado'] = FALSE;
        $this->session->set_userdata($dadosSessao);
        redirect(base_url('index.php/funcionarios/login'));
    }
}
