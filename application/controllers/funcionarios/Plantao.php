<?php


class Plantao extends CI_Controller
{

    
    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('logado')){
            redirect('funcionarios/login');
        }
    }

    public function lista_plantao()
    {
        $this->load->view('backend/template/html-header');
        $this->load->view('backend/template/templatefunc');

        $this->load->model('ModelFunc_Plantao', 'model');
        $data['card'] = $this->model->gera_card();

        $this->load->view('nsa/plantao/plantao.php', $data);
        $this->load->view('backend/template/html-footer');
    }
}