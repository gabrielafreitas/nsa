<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    
    
    class Nsa extends CI_Controller{
    
        public function index(){
            $this->load->view('nsa/common/header');
            $this->load->view('nsa/home/navbar');

            $this->load->view('nsa/home/carousel');
            $this->load->view('nsa/home/relogio');
            $this->load->view('nsa/home/quemsomos');
            
            $this->load->view('nsa/home/servicos');
            $this->load->view('nsa/home/fundo');
                 

            $this->load->view('nsa/home/footer');
            $this->load->view('nsa/common/footer');
        }
        
        public function contato(){
            $this->load->view('nsa/common/header');
            $this->load->view('nsa/common/navbar');

            $this->load->view('nsa/cliente/contato');
            $this->load->model('ModelCliente', 'model');
            $this->model->salva_cliente();
            $this->load->view('nsa/cliente/form_cliente');
            $this->load->view('nsa/home/fundo');
            
            

            $this->load->view('nsa/home/footer');
            $this->load->view('nsa/common/footer');
        }

        public function ambulancia(){

            $this->load->view('nsa/common/header');
            $this->load->view('nsa/common/navbar');
            $this->load->view('nsa/servicos/servicos');
            $this->load->view('nsa/servicos/ambulancias');
            $this->load->view('nsa/servicos/ambulancia_serv');
            
            $this->load->view('nsa/home/fundo');
            $this->load->view('nsa/home/footer');
            $this->load->view('nsa/common/footer');

        }
        
        public function remocao(){

            $this->load->view('nsa/common/header');
            $this->load->view('nsa/common/navbar');

            $this->load->view('nsa/servicos/servicos');
            $this->load->view('nsa/servicos/remocoes');
            $this->load->view('nsa/servicos/remocoes_serv');
            $this->load->view('nsa/home/fundo');
            $this->load->view('nsa/home/footer');
            $this->load->view('nsa/common/footer');

        }
        
        public function cobertura(){

            $this->load->view('nsa/common/header');
            $this->load->view('nsa/common/navbar');
            $this->load->view('nsa/servicos/servicos');
            $this->load->view('nsa/servicos/cobertura');
            $this->load->view('nsa/servicos/cobertura_serv');
            $this->load->view('nsa/home/fundo');
            $this->load->view('nsa/home/footer');
            $this->load->view('nsa/common/footer');

        }
        
     }
    
 