<?php


class Plantao extends CI_Controller
{

    
    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('logado')){
            redirect('index.php/administrativo/login');
        }
    }

    public function lista_plantao()
    {
        $this->load->view('backend/template/html-header');
        $this->load->view('backend/template/template');

        $this->load->model('PlantaoModel', 'model');
        $data['tabela'] = $this->model->gera_tabela();

        $this->load->view('nsa/plantao/table_plantao.php', $data);
        $this->load->view('backend/template/html-footer');
    }

    public function cadastro_plantao()
    {
        $this->load->view('backend/template/html-header');
        $this->load->view('backend/template/template');

        $this->load->model('PlantaoModel', 'model');
        $this->model->salva_plantao();

        $this->load->view('nsa/plantao/form_plantao');


        $this->load->view('backend/template/html-footer');
    }


    public function editar_plantao($id)
    {
        $this->load->view('backend/template/html-header');
        $this->load->view('backend/template/template');

        //carregar dados
        $this->load->model('PlantaoModel', 'model');
        $data['user'] = $this->model->read_plantao($id);

        //exibir na página
        $this->load->view('nsa/plantao/form_plantao', $data);

        //modificar e salvar
        $this->model->edita_plantao($id);
        $this->load->view('backend/template/html-footer');
    }



    public function deletar_plantao($id)
    {
        $this->load->model('PlantaoModel', 'model');
        $this->model->deletar_plantao($id);
        redirect('administrativo/plantao/lista_plantao');
    }
}
