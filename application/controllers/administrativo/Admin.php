 <?php

class Admin extends CI_Controller{

    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('logado')){
            redirect('administrativo/login');
        }
    }
    public function lista_admin(){
        if (!$this->session->userdata('logado')){
            redirect('index.php/administrativo/login');
        }    
    $this->load->view('backend/template/html-header');
    $this->load->view('backend/template/template');

    $this->load->model('ModelVisao', 'model');
    $data['tabela'] = $this->model->gera_tabela_admin();
    $this->load->view('nsa/admin/table_admin.php', $data);
    $this->load->view('backend/template/html-footer');
}

   
   public function cadastro_admin()
    {
        if (!$this->session->userdata('logado')){
            redirect('index.php/administrativo/login');
        }
        $this->load->view('backend/template/html-header');
        $this->load->view('backend/template/template');

        $this->load->model('ModelVisao', 'model');
        $this->model->salva_admin();

        $this->load->view('nsa/admin/form_admin.php');

        $this->load->view('backend/template/html-footer');
    }

    
    public function edita_admin($id)
    {
        if (!$this->session->userdata('logado')){
            redirect('index.php/administrativo/login');
        }
        $this->load->view('backend/template/html-header');
        $this->load->view('backend/template/template');

        $this->load->model('ModelVisao', 'model');
        $data['user'] = $this->model->read_admin($id);

        $this->load->view('nsa/admin/form_admin.php', $data);

        $this->model->edita_admin($id);

        $this->load->view('backend/template/html-footer');
    }



    public function delete_admin($id)
    {
        if (!$this->session->userdata('logado')){
            redirect('index.php/administrativo/login');
        }
        $this->load->model('ModelVisao', 'model');
        $this->model->delete_admin($id);
        redirect('administrativo/admin/lista_admin');
    }

    public function lista_jan(){
        if (!$this->session->userdata('logado')){
            redirect('index.php/administrativo/login');
        }
        
        $this->load->view('backend/template/html-header');
        $this->load->view('backend/template/template');

        //$this->load->model('ModelVisao', 'model');
        //$resultado = $this->model->gera_mes_jan();
        
        $this->load->model('ModelVisao', 'model');
        $data['mes'] = $this->model->gera_mes_jan();
        $this->load->view('nsa/admin/table_month.php', $data);
        //print_r($resultado);
        //var_dump($resultado);

        $this->load->view('backend/template/html-footer');

    }
    public function lista_fev(){
        if (!$this->session->userdata('logado')){
            redirect('index.php/administrativo/login');
        }
        
        $this->load->view('backend/template/html-header');
        $this->load->view('backend/template/template');
        $this->load->model('ModelVisao', 'model');
        $data['mes'] = $this->model->gera_mes_fev();
        $this->load->view('nsa/admin/table_month.php', $data);
        $this->load->view('backend/template/html-footer');

    }
    public function lista_mar(){
        if (!$this->session->userdata('logado')){
            redirect('index.php/administrativo/login');
        }
        
        $this->load->view('backend/template/html-header');
        $this->load->view('backend/template/template');
        $this->load->model('ModelVisao', 'model');
        $data['mes'] = $this->model->gera_mes_mar();
        $this->load->view('nsa/admin/table_month.php', $data);
        $this->load->view('backend/template/html-footer');

    }
    public function lista_abr(){
        if (!$this->session->userdata('logado')){
            redirect('index.php/administrativo/login');
        }
        
        $this->load->view('backend/template/html-header');
        $this->load->view('backend/template/template');
        $this->load->model('ModelVisao', 'model');
        $data['mes'] = $this->model->gera_mes_abr();
        $this->load->view('nsa/admin/table_month.php', $data);
        $this->load->view('backend/template/html-footer');

    }
    public function lista_mai(){
        if (!$this->session->userdata('logado')){
            redirect('index.php/administrativo/login');
        }
        
        $this->load->view('backend/template/html-header');
        $this->load->view('backend/template/template');
        $this->load->model('ModelVisao', 'model');
        $data['mes'] = $this->model->gera_mes_mai();
        $this->load->view('nsa/admin/table_month.php', $data);
        $this->load->view('backend/template/html-footer');

    }
    public function lista_jun(){
        if (!$this->session->userdata('logado')){
            redirect('index.php/administrativo/login');
        }
        
        $this->load->view('backend/template/html-header');
        $this->load->view('backend/template/template');
        $this->load->model('ModelVisao', 'model');
        $data['mes'] = $this->model->gera_mes_jun();
        $this->load->view('nsa/admin/table_month.php', $data);
        $this->load->view('backend/template/html-footer');

    }
    public function lista_jul(){
        if (!$this->session->userdata('logado')){
            redirect('index.php/administrativo/login');
        }
        
        $this->load->view('backend/template/html-header');
        $this->load->view('backend/template/template');
        $this->load->model('ModelVisao', 'model');
        $data['mes'] = $this->model->gera_mes_jul();
        $this->load->view('nsa/admin/table_month.php', $data);
        $this->load->view('backend/template/html-footer');

    }
    public function lista_ago(){
        if (!$this->session->userdata('logado')){
            redirect('index.php/administrativo/login');
        }
        
        $this->load->view('backend/template/html-header');
        $this->load->view('backend/template/template');
        $this->load->model('ModelVisao', 'model');
        $data['mes'] = $this->model->gera_mes_ago();
        $this->load->view('nsa/admin/table_month.php', $data);
        $this->load->view('backend/template/html-footer');

    }
    public function lista_set(){
        if (!$this->session->userdata('logado')){
            redirect('index.php/administrativo/login');
        }
        
        $this->load->view('backend/template/html-header');
        $this->load->view('backend/template/template');
        $this->load->model('ModelVisao', 'model');
        $data['mes'] = $this->model->gera_mes_set();
        $this->load->view('nsa/admin/table_month.php', $data);
        $this->load->view('backend/template/html-footer');

    }
    public function lista_out(){
        if (!$this->session->userdata('logado')){
            redirect('index.php/administrativo/login');
        }
        
        $this->load->view('backend/template/html-header');
        $this->load->view('backend/template/template');
        $this->load->model('ModelVisao', 'model');
        $data['mes'] = $this->model->gera_mes_out();
        $this->load->view('nsa/admin/table_month.php', $data);
        $this->load->view('backend/template/html-footer');

    }
    public function lista_nov(){
        if (!$this->session->userdata('logado')){
            redirect('index.php/administrativo/login');
        }
        
        $this->load->view('backend/template/html-header');
        $this->load->view('backend/template/template');
        $this->load->model('ModelVisao', 'model');
        $data['mes'] = $this->model->gera_mes_nov();
        $this->load->view('nsa/admin/table_month.php', $data);
        $this->load->view('backend/template/html-footer');

    }
    public function lista_dez(){
        if (!$this->session->userdata('logado')){
            redirect('index.php/administrativo/login');
        }
        
        $this->load->view('backend/template/html-header');
        $this->load->view('backend/template/template');
        $this->load->model('ModelVisao', 'model');
        $data['mes'] = $this->model->gera_mes_dez();
        $this->load->view('nsa/admin/table_month.php', $data);
        $this->load->view('backend/template/html-footer');

    }





}