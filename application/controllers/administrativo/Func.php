<?php

class Func extends CI_Controller
{

     
    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('logado')){
            redirect('index.php/administrativo/login');
        }
    }

    public function lista_func()
    {
        $this->load->view('backend/template/html-header');
        $this->load->view('backend/template/template');

        $this->load->model('ModelFunc', 'model');
        $data['tabela'] = $this->model->gera_tabela_func();
        $this->load->view('nsa/funcionario/table_func.php', $data);


        $this->load->view('backend/template/html-footer');
    }


    public function cadastro_func()
    {
        $this->load->view('backend/template/html-header');
        $this->load->view('backend/template/template');

        $this->load->model('ModelFunc', 'model');
        $this->model->salva_func();

        $this->load->view('nsa/funcionario/form_func.php');


        $this->load->view('backend/template/html-footer');
    }

    
    public function editar_func($id)
    {
        $this->load->view('backend/template/html-header');
        $this->load->view('backend/template/template');

        $this->load->model('ModelFunc', 'model');
        $data['user'] = $this->model->read_func($id);

        $this->load->view('nsa/funcionario/form_func.php', $data);

        $this->model->edita_func($id);

        $this->load->view('backend/template/html-footer');
    }



    public function deletar_func($id)
    {
        $this->load->model('ModelFunc', 'model');
        $this->model->deletar_func($id);
        redirect('administrativo/func/lista_func');
    }
}
