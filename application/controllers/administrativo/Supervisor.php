
   <?php

class Supervisor extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('logado')){
            redirect('index.php/administrativo/login');
        }
    }
    public function lista_super(){
        if (!$this->session->userdata('logado')){
            redirect('index.php/administrativo/login');
        }
    
    $this->load->view('backend/template/html-header');
    $this->load->view('backend/template/template');

    $this->load->model('ModelVisao', 'model');
    $data['tabela'] = $this->model->gera_tabela();
    $this->load->view('nsa/supervisor/table_super.php', $data);


    $this->load->view('backend/template/html-footer');
}

   
   public function cadastro_super()
    {
        if (!$this->session->userdata('logado')){
            redirect('index.php/administrativo/login');
        }
        $this->load->view('backend/template/html-header');
        $this->load->view('backend/template/template');

        $this->load->model('ModelVisao', 'model');
        $this->model->salva_super();

        $this->load->view('nsa/supervisor/form_super.php');


        $this->load->view('backend/template/html-footer');
    }

    
    public function edita_super($id)
    {
        if (!$this->session->userdata('logado')){
            redirect('index.php/administrativo/login');
        }
        $this->load->view('backend/template/html-header');
        $this->load->view('backend/template/template');

        $this->load->model('ModelVisao', 'model');
        $data['user'] = $this->model->read_super($id);

        $this->load->view('nsa/supervisor/form_super.php', $data);

        $this->model->edita_super($id);

        $this->load->view('backend/template/html-footer');
    }



    public function delete_super($id)
    {
        if (!$this->session->userdata('logado')){
            redirect('index.php/administrativo/login');
        }
        $this->load->model('ModelVisao', 'model');
        $this->model->delete_super($id);
        redirect('administrativo/supervisor/lista_super');
    }
}
