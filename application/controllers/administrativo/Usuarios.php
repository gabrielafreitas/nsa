<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Usuarios extends CI_Controller
{
    public function pag_login()
    {
        $this->load->view('backend/template/html-header');
        $this->load->view('backend/login');
        $this->load->view('backend/template/html-footer');
    }

    public function login()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('txt-user', 'Usuário', 'required|min_length[3]');
        $this->form_validation->set_rules('txt-senha', 'Senha', 'required|min_length[3]');

        if ($this->form_validation->run() == FALSE) {
            $this->pag_login();
        } else {
            $usuario = $this->input->post('txt-user');
            $senha = $this->input->post('txt-senha');
            $this->db->where('user', $usuario);
            $this->db->where('senha', md5($senha));
            $userlogado2 = $this->db->get('usuario')->result();

            if (count($userlogado2) == 1) {
                $dadosSessao['userlogado2'] = $userlogado2[0];
                $dadosSessao['logado'] = TRUE;
                $this->session->set_userdata($dadosSessao);
                redirect(base_url('index.php/administrativo/home'));
            } else {
                $dadosSessao['userlogado2'] = NULL;
                $dadosSessao['logado'] = FALSE;
                $this->session->set_userdata($dadosSessao);
                redirect(base_url('index.php/administrativo/login'));
                
            }
        }
    }

    public function logout()
    {
        $dadosSessao['userlogado2'] = NULL;
        $dadosSessao['logado'] = FALSE;
        $this->session->set_userdata($dadosSessao);
        redirect(base_url('index.php/administrativo/login'));
    }
}
