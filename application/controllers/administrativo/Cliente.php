<?php

class Cliente extends CI_Controller
{


    
    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('logado')){
            redirect('index.php/administrativo/login');
        }
    }

    
    public function lista_cliente()
    {
        $this->load->view('backend/template/html-header');
        $this->load->view('backend/template/template');

        $this->load->model('ModelCliente', 'model');
        $data['tabela'] = $this->model->gera_tabela_cliente();
        $this->load->view('nsa/cliente/table_cliente.php', $data);


        $this->load->view('backend/template/html-footer');
    }


    public function deletar_cliente($id)
    {
        $this->load->model('ModelCliente', 'model');
        $this->model->deletar_cliente($id);
        redirect('administrativo/cliente/lista_cliente');
    }
}
