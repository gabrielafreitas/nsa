<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Supervisor extends CI_Controller
{
    public function pag_login()
    {
        $this->load->view('backend/template/html-header');
        $this->load->view('backend/loginsuper');
        $this->load->view('backend/template/html-footer');
    }
    public function login()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('txt-user', 'Usuário', 'required|min_length[3]');
        $this->form_validation->set_rules('txt-senha', 'Senha', 'required|min_length[3]');

        if ($this->form_validation->run() == FALSE) {
            $this->pag_login();
        } else {
            $usuario = $this->input->post('txt-user');
            $senha = $this->input->post('txt-senha');
            $this->db->where('user', $usuario);
            $this->db->where('senha', md5($senha));
            $userlogado = $this->db->get('supervisao')->result();

            if (count($userlogado) == 1) {
                $dadosSessao['userlogado'] = $userlogado[0];
                $dadosSessao['logado'] = TRUE;
                $this->session->set_userdata($dadosSessao);
                redirect('supervisao/plantao/home');
            } else {
                $dadosSessao['userlogado'] = NULL;
                $dadosSessao['logado'] = FALSE;
                $this->session->set_userdata($dadosSessao);
                redirect(base_url('index.php/supervisao/login'));
            }
        }
    }

    public function logout()
    {
        $dadosSessao['superlogado'] = NULL;
        $dadosSessao['logado'] = FALSE;
        $this->session->set_userdata($dadosSessao);
        redirect(base_url('index.php/supervisao/login'));
    }
}
