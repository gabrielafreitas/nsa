<header>

  <nav class="navbar navbar-dark red">  
  </nav>
    <nav class="navbar fixed-top navbar-expand-lg navbar-dark rgba(255, 255, 255, 0.3) rgba-white-light" >
      <div class="container">

        <a class="navbar-brand waves-effect">
         <strong class="blue-text"><img src="<?= base_url('assets/mdb/img/icons/icon.png') ?>" width="70px"/></strong>
        </a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon red"></span>
        </button>

        <div class="collapse navbar-collapse " id="navbarSupportedContent" >

         <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
              <a class="nav-link waves-effect" href="<?=base_url('plantao/lista_plantao')?>"><button type="button" class="btn btn-danger">Plantão</button>
                <span class="sr-only">(current)</span>
              </a>
            </li>
          </ul> 

          <ul class="navbar-nav nav-flex-icons">
            <li class="nav-item">
              <a href="<?=base_url('login/supervisor')?>" class="nav-link waves-effect" target="_blank">
              <span class="badge badge-primary"><i class="fas fa-users-cog"> Supervisor</i></span>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?=base_url('login/administrador')?>" class="nav-link waves-effect" target="_blank">
              <span class="badge badge-danger"><i class="fas fa-user-lock"> Administrador</i></span>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?=base_url('login/funcionario')?>" class="nav-link waves-effect" target="_blank">
              <span class="badge badge-primary"><i class="fas fa-users-cog"> Funcionário</i></span>
              </a>
            </li>
          </ul>
        </div>

      </div>
    </nav>
    