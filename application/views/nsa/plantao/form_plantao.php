<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header ">Cadastrar novo plantão</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form class="text-center border border-light p-5" method="POST">
                                <input value="<?= isset($user) ? $user['local'] : '' ?>" type="text" id="local" name="local" class="form-control mb-4" placeholder="Local do plantão"><br>
                                <input value="<?= isset($user) ? $user['data'] : '' ?>" type="date" id="data" name="data" class="form-control mb-4" placeholder="Data"><br>
                                <input value="<?= isset($user) ? $user['horario'] : '' ?>" type="time" id="horario" name="horario" class="form-control mb-4" placeholder="Horário"><br>
                                <input value="<?= isset($user) ? $user['descricao'] : '' ?>" type="text" id="descricao" name="descricao" class="form-control mb-4" placeholder="Descrição"><br>
                                <button class="btn btn-info btn-block my-4" type="submit">Enviar</button>
                            </form>
                        </div>

                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->

</div>