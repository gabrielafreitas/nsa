<div id="page-wrapper">
  <div class="row">
    <div class="col-lg-12">
      <h1 class="page-header ">Plantões</h1>
    </div>
    <!-- /.col-lg-12 -->
  </div>
  <!-- /.row -->
  <div class="row">
    <div class="col-md-9">
      <div class="panel panel-default">
        <div class="panel-body">
          <div class="row">
            <div class="col-md-15">
              <table class="table">
                <thead class="red white-text">
                  <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Local</th>
                    <th scope="col">Data</th>
                    <th scope="col">Horário</th>
                    <th scope="col">Descrição</th>
                   
                  </tr>
                </thead>
                <tbody>
                  <?= $tabela ?>
                </tbody>
              </table>

            </div>

          </div>
          <!-- /.row (nested) -->
        </div>
        <!-- /.panel-body -->
      </div>
      <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
  </div>
  <!-- /.row -->
</div>
<!-- /#page-wrapper -->

</div>