<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header ">Cadastrar novo Administrador</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form class="border border-light p-5" method="POST">
                                <p class="h4 mb-4">Dados do Administrador</p>
                                <input value="<?= isset($user) ? $user['nome'] : '' ?>" type="text" id="nome" name="nome" class="form-control mb-4" placeholder="Nome">
                                <p class="h4 mb-4">Dados de contato</p>
                                <input value="<?= isset($user) ? $user['email'] : '' ?>" type="email" id="email" name="email" class="form-control mb-4" placeholder="E-Mail">
                                <input value="<?= isset($user) ? $user['user'] : '' ?>" type="text" id="user" name="user" class="form-control mb-4" placeholder="User">
                                <br>
                                <p class="h4 mb-4">Senha</p>
                                <input value="<?= isset($user) ? $user['senha'] : '' ?>" type="password" id="senha" name="senha" class="form-control mb-4" placeholder="Senha">
                                <br><button class="btn btn-info btn-block my-4" type="submit">Enviar</button>
                            </form>
                        </div>

                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->

</div>