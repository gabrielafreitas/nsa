<div id="page-wrapper">
  <div class="row">
    <div class="col-lg-12">
      <h1 class="page-header ">Andamento dos plantões por mês</h1>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-body">
          <div class="row">
            <div class="col-md-10">
              <table class="table">
                <thead class="red white-text">
                  <tr>
                  
                    <th scope="col">Nº de Plantões Realizados</th>
                    <th scope="col">Mês</th>
                                
                  </tr>
                </thead>
                <tbody>
                  <?= $mes ?>
                </tbody>
              </table>
            </div>
          </div> 
        </div>   
      </div> 
    </div>
  </div>
</div>
