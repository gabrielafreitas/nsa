<div id="page-wrapper">
  <div class="row">
    <div class="col-lg-12">
      <h1 class="page-header ">Administradores</h1>
    </div>
    <!-- /.col-lg-12 -->
  </div>
  <!-- /.row -->
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-body">
          <div class="row">
            <div class="col-md-10">
              <table class="table">
                <thead class="red white-text">
                  <tr>
                  
                    <th scope="col">Nome</th>
                    <th scope="col">E-Mail</th>
                    <th scope="col">User</th>                  
                  </tr>
                </thead>
                <tbody>
                  <?= $tabela ?>
                </tbody>
              </table>

            </div>
          </div>
          <!-- /.row (nested) -->
        </div>
        <!-- /.panel-body -->
      </div>
      <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
  </div>
  <!-- /.row -->
</div>
<!-- /#page-wrapper -->