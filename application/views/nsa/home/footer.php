<hr>
<footer class="page-footer font-small rgba(255, 255, 255, 0.3) rgba-white-light pt-4" id="footer">

  
  <div class="container-fluid text-center text-md-left">

    
    <div class="row">

      
      <div class="col-md-5 mt-md-0 mt-3">
		<div class="col-md-12">
            <h5 class=" font-weight-bold">NSA Emergências Médicas</h5>
            <p class="text-dark">Prestamos serviços de cobertura médica em eventos, oferecemos ambulâncias UTI e Básicas, posto médico e equipe médica.</p>
            <p class="text-dark">Nossa proposta é garantir o apoio médico, proporcionando tranquilidade e segurança aos seus eventos.</p>
            <p class="text-dark">Contamos com profissionais altamente especializados em eventos sociais, esportivos e culturais de pequeno, médio e grande porte Ambulatório Móvel.</p>   
        </div>
      </div>

      <div class="col-md-3 mt-md-0 mt-3">
        
        <div class="col-md-12">
            <h5  class=" font-weight-bold">Onde Atendemos</h5> <br>
               <dl>
					<dt class="text-dark">ESTADO DE SÃO PAULO</dt> <br />
					<dd class="text-dark">Grande São Paulo</dd>
					<dd class="text-dark">Litoral</dd>
					<dd class="text-dark">Interior</dd>
			   </dl>
               
        </div>        

      </div>
      <div class="col-md-4 mt-md-0 mt-3">

        <center> <img src="<?= base_url('assets/mdb/img/icons/logo2.png') ?>" width="350px" height="200px" /> </center>		
        <br />
        
      </div>      
    </div>   
  </div>
  
  <div class="footer-copyright text-center red py-3">
  <ul class="list-unstyled list-inline text-center">
    <li class="list-inline-item">
      <a class="btn-floating btn-fb mx-1" href="https://www.facebook.com/NSA-Remo%C3%A7%C3%B5es-M%C3%A9dicas-597383127380778/">
      <span class="badge badge-primary"><i class="fab fa-facebook-f" aria-hidden="true"></i></span>
      </a>
    </li>
    <li class="list-inline-item">
      <a class="btn-floating btn-tw mx-1" href="mailto:nsambulancias@hotmail.com">
      <span class="badge badge-danger"><i class="far fa-envelope"></i></i></span>
      </a>
    </li>
    <li class="list-inline-item">
      <a class="btn-floating btn-gplus mx-1" href="https://api.whatsapp.com/send?1=pt_BR&phone=5511958656086&text=Olá,%20gostaria%20de%20entrar%20em%20contato%20com%20a%20NSA%20Emergências%20Médicas!">
      <span class="badge badge-success"><i class="fab fa-whatsapp" aria-hidden="true"></i></span>
      </a>
    </li>
  </ul>
  <br>
  ACESSO RESTRITO <br>
      <a href="<?=base_url('index.php/supervisao/login')?>" class="nav-link waves-effect" target="_blank">
        <span class="badge badge-primary"><i class="fas fa-users-cog"> Supervisor</i></span>
      </a>
      <a href="<?=base_url('index.php/administrativo/login')?>" class="nav-link waves-effect" target="_blank">
        <span class="badge badge-danger"><i class="fas fa-user-lock"> Administrador</i></span>
      </a>
      <a href="<?=base_url('index.php/funcionarios/login')?>" class="nav-link waves-effect" target="_blank">
        <span class="badge badge-primary"><i class="fas fa-users-cog"> Funcionário</i></span>
      </a>
  <br>
    © 2019 Copyright:
    <a href="http://hospedagem.ifspguarulhos.edu.br/~gu1800281/setup/"><img src="<?= base_url('assets/mdb/img/icons/seta.png') ?>" width="40px"></a>
    <br>  Todos os direitos reservados a NSA Emergências Médicas 
  </div>
  <br>
  <a  href="#"><i class="fas fa-arrow-up text-primary"></i></a>
  
</footer>
