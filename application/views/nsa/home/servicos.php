<section id="service" class="services-mf route">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="title-box text-center">
            <h3 class="title-a">
              Serviços
            </h3>
            <p class="subtitle-a">
                Nossos serviços de qualidade.
            </p>
            <div class="line-mf"></div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4">
          <div class="service-box">
            <div class="service-ico">
              <a href="<?=base_url('index.php/nsa/ambulancia')?>"><span class="ico-circle #ef9a9a "><img src="<?= base_url('assets/mdb/img/servicos/ambulancia.png') ?>" width="40px"></span></a>
            </div>
            <div class="service-content">
              <h2 class="s-title">Ambulâncias</h2>
            </div>
          </div>
        </div>
        
        <div class="col-md-4">
          <div class="service-box">
            <div class="service-ico">
              <a href="<?=base_url('index.php/nsa/cobertura')?>"><span class="ico-circle #ef9a9a "><img src="<?= base_url('assets/mdb/img/servicos/icon-eventos.png') ?>" width="40px"></span></a>
            </div>
            <div class="service-content">
              <h2 class="s-title">Cobertura de Eventos</h2>
            
            </div>
          </div>
        </div>
       
        <div class="col-md-4">
          <div class="service-box">
            <div class="service-ico">
             <a href="<?=base_url('index.php/nsa/remocao')?>"><span class="ico-circle #ef9a9a"><img src="<?= base_url('assets/mdb/img/servicos/maca.png') ?>" width="40px"></span></a>
            </div>
            <div class="service-content">
              <h2 class="s-title">Remoções Hospitalares</h2>
              
            </div>
          </div>
        </div>
        
      </div>
    </div>
  </section>