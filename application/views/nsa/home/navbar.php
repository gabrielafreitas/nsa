<header>

  <nav class="navbar navbar-dark red">
  
  </nav>

    <nav class="navbar fixed-top navbar-expand-lg navbar-dark rgba(255, 255, 255, 0.3) rgba-white-light scrolling-navbar" >
      <div class="container">

        <a class="navbar-brand waves-effect" href="<?=base_url('/')?>">
         <strong class="blue-text"><img src="<?= base_url('assets/mdb/img/icons/icon.png') ?>" width="70px"/></strong>
        </a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon red"></span>
        </button>

        <div class="collapse navbar-collapse " id="navbarSupportedContent" >

          <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
              <a class="nav-link waves-effect" href="<?=base_url('/')?>"><button type="button" class="btn btn-danger">Home</button>
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link waves-effect" href="#footer" ><button type="button" class="btn primary-color-dark">Quem Somos</button></a>
            </li>
            <li class="nav-item">
              <a class="nav-link waves-effect" href="#service" ><button type="button" class="btn primary-color-dark">Serviços</button></a>
            </li>
            
            <li class="nav-item">
              <a class="nav-link waves-effect" href="<?=base_url('index.php/nsa/contato')?>"><button type="button" class="btn btn-danger">Contato</button></a>
            </li>
          </ul>

          <ul class="navbar-nav nav-flex-icons">
            <li class="nav-item">
              <a href="https://www.facebook.com/NSA-Remo%C3%A7%C3%B5es-M%C3%A9dicas-597383127380778/" class="nav-link waves-effect" target="_blank">
              <span class="badge badge-primary"><i class="fab fa-facebook-f" aria-hidden="true"></i></span>
              </a>
            </li>
            <li class="nav-item">
              <a href="mailto:nsambulancias@hotmail.com" class="nav-link waves-effect" target="_blank">
              <span class="badge badge-danger"><i class="far fa-envelope"></i></i></span>
              </a>
            </li>
            <li class="nav-item">
              <a href="https://api.whatsapp.com/send?1=pt_BR&phone=5511958656086&text=Olá,%20gostaria%20de%20entrar%20em%20contato%20com%20a%20NSA%20Emergências%20Médicas!" class="nav-link waves-effect"
                target="_blank">
                <span class="badge badge-success"><i class="fab fa-whatsapp" aria-hidden="true"></i></span>
              </a>
            </li>
          </ul>

        </div>

      </div>
    </nav>
    