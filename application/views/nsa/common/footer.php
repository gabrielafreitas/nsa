<script type="text/javascript" src="<?= base_url('assets/mdb/js/jquery-3.4.1.min.js')?>"></script>
  <script type="text/javascript" src="<?= base_url('assets/mdb/js/popper.min.js')?>"></script>
  <script type="text/javascript" src="<?= base_url('assets/mdb/js/bootstrap.min.js')?>"></script>
  <script type="text/javascript" src="<?= base_url('assets/mdb/js/mdb.min.js')?>"></script>
  
  <script type="text/javascript" src="<?= base_url('assets/js/owl.carousel.min.js')?>"></script>

  <script src="<?= base_url('lib/jquery/jquery.min.js')?>"></script>
  <script src="<?= base_url('lib/jquery/jquery-migrate.min.js')?>"></script>
  <script src="<?= base_url('lib/popper/popper.min.js')?>"></script>
  <script src="<?= base_url('lib/bootstrap/js/bootstrap.min.js')?>"></script>
  <script src="<?= base_url('lib/easing/easing.min.js')?>"></script>
  <script src="<?= base_url('lib/counterup/jquery.waypoints.min.js')?>"></script>
  <script src="<?= base_url('lib/counterup/jquery.counterup.js')?>"></script>
 
  <script src="<?= base_url('lib/lightbox/js/lightbox.min.js')?>"></script>
  <script src="<?= base_url('lib/typed/typed.min.js')?>"></script>


  <script src="<?= base_url('contactform/contactform.js')?>"></script>
  <script src="<?= base_url('js/main.js')?>"></script>
</body>

</html>