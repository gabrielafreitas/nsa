<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>NSA | Emergências Médicas</title>
  <link rel="shortcut icon" href="<?= base_url('assets/mdb/img/icons/icon.png') ?>" type="image/x-icon" />
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
  <link href="<?= base_url('assets/mdb/css/bootstrap.min.css')?>" rel="stylesheet">
  <link href="<?= base_url('assets/mdb/css/mdb.min.css')?>" rel="stylesheet">
  <link href="<?= base_url('assets/mdb/css/style.css')?>" rel="stylesheet">
  <link href="<?= base_url('assets/mdb/css/tamplete/style.css')?>" rel="stylesheet">

  <link href="<?= base_url('assets/css/owl.carousel.min.css')?>" rel="stylesheet">
</head>

<body>