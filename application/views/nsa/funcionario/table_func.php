<div id="page-wrapper">
  <div class="row">
    <div class="col-lg-12">
      <h1 class="page-header ">Funcionários</h1>
    </div>
    <!-- /.col-lg-12 -->
  </div>
  <!-- /.row -->
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-body">
          <div class="row">
            <div class="col-md-10">
              <table class="table">
                <thead class="red white-text">
                  <tr>
                    <th scope="col">Nº</th>
                    <th scope="col">Nome</th>
                    <th scope="col">Sobrenome</th>
                    <th scope="col">Data de Nascimento</th>
                    <th scope="col">RG</th>
                    <th scope="col">E-Mail</th>
                    <th scope="col">Telefone</th>
                    <th scope="col">Endereço</th>
                    <th scope="col">Área</th>                    
                  </tr>
                </thead>
                <tbody>
                  <?= $tabela ?>
                </tbody>
              </table>

            </div>
          </div>
          <!-- /.row (nested) -->
        </div>
        <!-- /.panel-body -->
      </div>
      <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
  </div>
  <!-- /.row -->
</div>
<!-- /#page-wrapper -->