<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header ">Cadastrar novo funcionário</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form class="border border-light p-5" method="POST">
                                <p class="h4 mb-4">Dados do funcionário</p>
                                <input value="<?= isset($user) ? $user['nome'] : '' ?>" type="text" id="nome" name="nome" class="form-control mb-4" placeholder="Nome">
                                <input value="<?= isset($user) ? $user['sobrenome'] : '' ?>" type="text" id="sobrenome" name="sobrenome" class="form-control mb-4" placeholder="Sobrenome">
                                <input value="<?= isset($user) ? $user['dt_nasc'] : '' ?>" type="date" id="dt_nasc" name="dt_nasc" class="form-control mb-4" placeholder="Data de Nascimento">
                                <input value="<?= isset($user) ? $user['rg'] : '' ?>" type="number" id="rg" name="rg" class="form-control mb-4" placeholder="RG">
                                <br>
                                <p class="h4 mb-4">Dados de contato</p>
                                <input value="<?= isset($user) ? $user['email'] : '' ?>" type="email" id="email" name="email" class="form-control mb-4" placeholder="E-Mail">
                                <input value="<?= isset($user) ? $user['telefone'] : '' ?>" type="number" id="telefone" name="telefone" class="form-control mb-4" placeholder="Telefone">
                                <br>
                                <p class="h4 mb-4">Endereço</p>
                                <input value="<?= isset($user) ? $user['endereco'] : '' ?>" type="text" id="endereco" name="endereco" class="form-control mb-4" placeholder="Endereço">
                                <br>
                                <label>Área de atuação</label>
                                <select class="form-control" name='area'>
                                    <option disabled selected="selected">Escolha a área</option>
                                    <option value="Médico(a)" >Médico(a)</option>
                                    <option value="Socorrista">Socorrista</option>
                                    <option value="Enfermeiro(a)">Enfermeiro(a)</option>
                                    <option value="Paramédico(a)">Paramédico(a)</option>
                                </select> 
                                    <br><button class="btn btn-info btn-block my-4" type="submit">Enviar</button>
                            </form>
                        </div>

                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->

</div>