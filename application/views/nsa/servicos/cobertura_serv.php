<div class="col-md-10 mx-auto">
    <section class="my-5">
    <div class="row">
      <div class="col-lg-5">
        <div class="view overlay rounded z-depth-2 mb-lg-0 mb-4">
          <img class="img-fluid" src="<?= base_url('assets/mdb/img/servicos/cobertura1.jpg') ?>" alt="Sample image">
          <a>
            <div class="mask rgba-white-slight"></div>
          </a>
        </div>
      </div>
      <div class="col-lg-7">
        <a href="#!" class="green-text">
          <h6 class="font-weight-bold mb-3"><img src="<?= base_url('assets/mdb/img/servicos/icon-eventos.png') ?>" width="30 px"></h6>
        </a>
        <h1 class="font-weight-bold mb-3"><strong>Cobertura de Eventos</strong></h1>
        <h5>Cobertura de eventos como: shows, festivais, passeatas, eventos de ciclismo, etc. Atendendo o estado de São Paulo.</h5>
        
      </div>
    </div>
    <hr class="my-5">
    <div class="row">
      <div class="col-lg-7">
        <a href="#!" class="pink-text">
          <h6 class="font-weight-bold mb-3"><img src="<?= base_url('assets/mdb/img/servicos/icon-eventos.png') ?>" width="30 px"></h6>
        </a>
        <h1 class="font-weight-bold mb-3"><strong>Área Protegida</strong></h1>
        <h5>Sistema de emergências médicas, para que haja possibilidade de atendimetno imediato em unidade se suporte básicos e avançados.</h5>
    </div>
      <div class="col-lg-5">
        <div class="view overlay rounded z-depth-2">
          <img class="img-fluid" src="<?= base_url('assets/mdb/img/servicos/cobertura3.jpg') ?>" alt="Sample image">
          <a>
            <div class="mask rgba-white-slight"></div>
          </a>
        </div>
      </div>

    </div>    
    </section>

</div>