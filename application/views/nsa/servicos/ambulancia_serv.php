<div class="col-md-10 mx-auto">
    <section class="my-5">
        <div class="row">
        <div class="col-lg-5">
            <div class="view overlay rounded z-depth-2 mb-lg-0 mb-4">
            <img class="img-fluid" src="<?= base_url('assets/mdb/img/servicos/ambulancia2.jpg') ?>" alt="Ambulancia 1">
            <a>
                <div class="mask rgba-white-slight"></div>
            </a>
            </div>
        </div>
        <div class="col-lg-7">
            <a href="#!" class="green-text">
            <h6 class="font-weight-bold mb-3"><img src="<?= base_url('assets/mdb/img/servicos/ambulancia.png') ?>" width="30 px"></h6>
            </a>
            <h1 class="font-weight-bold mb-3"><strong>Ambulância UTI</strong></h1>
            <h5>Ambulância do tipo D, é a ambulância de suporte avançado (UTI móvel). Utilizada para o transporte de pacientes graves.
             O veículo compõe o sistema de atendimento pré-hospitalar e o transporte inter-hospitalar.</h5>
        </div>
        </div>
        <hr class="my-5">
        <div class="row">
        <div class="col-lg-7">
            <a href="#!" class="pink-text">
            <h6 class="font-weight-bold mb-3"><img src="<?= base_url('assets/mdb/img/servicos/ambulancia.png') ?>" width="30 px"></h6>
            </a>
            <h1 class="font-weight-bold mb-3"><strong>Ambulância Básica</strong></h1>
            <h5>Ambulância do tipo B, é a ambulância de suporte básico. Utilizado para transportar pacientes
             com algum risco de vida e inter hospitalar de pacientes. Contendo equipamentos mínimos para a manutenção da vida.</h5>
        </div>
        <div class="col-lg-5">
            <div class="view overlay rounded z-depth-2">
            <img class="img-fluid" src="<?= base_url('assets/mdb/img/servicos/ambulancia1.jfif') ?>" width="700 px" height ="600px" alt="Ambulancia 2">
            <a>
                <div class="mask rgba-white-slight"></div>
            </a>
            </div>
        </div>

        </div>
    </section>
</div>