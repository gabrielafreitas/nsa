<div class="col-md-8 mx-auto">
  <form method="POST">
    <section class="contact-section" id="contato">
      <?= validation_errors('<div class="alert alert-danger">', '</div>') ?>;
      <div class="col-lg-12">
        <div class="card-body form">

          <div class="row">
            <div class="col-md-6">
              <div class="md-form mb-0">
                <input value="<?= isset($user) ? $user['nome'] : '' ?>" type="text" id="nome" name="nome" class="form-control">
                <label for="nome">Seu Nome</label>
              </div>
            </div>
            <div class="col-md-6">
              <div class="md-form mb-0">
                <input value="<?= isset($user) ? $user['email'] : '' ?>" type="email" id="email" name="email" class="form-control">
                <label for="email">Seu E-mail</label>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="md-form mb-0">
                <input value="<?= isset($user) ? $user['telefone'] : '' ?>" type="number" id="telefone" name="telefone" class="form-control">
                <label for="telephone" class="">Seu Telefone</label>
              </div>
            </div>
            <div class="col-md-6">
              <div class="md-form mb-0">
                <input value="<?= isset($user) ? $user['empresa'] : '' ?>" type="text" id="empresa" name="empresa" class="form-control">
                <label for="empresa" class="">A Empresa</label>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="md-form mb-0">
                <input value="<?= isset($user) ? $user['mensagem'] : '' ?>" type="text" id="mensagem" name="mensagem" class="form-control" row="3">
                <label for="mensagem">Sua Mensagem</label>
                <button class="btn primary-color btn-lg  text-dark" type="submit"><i class="fa fa-paper-plane" aria-hidden="true"></i> ENVIAR</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </form>
</div>