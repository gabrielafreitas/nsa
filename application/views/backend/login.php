<style type="text/css">
body {
    background-image: url(<?=base_url('assets/mdb/img/emergence.jpg')?>);  
    
  
}
.fundo{
    background-color: #dcf4f3;
}
</style>

<div class="container fundo">
        <div class="row">
            <br>
            <h3 class="col-md-offset-5">Olá, Administrador!</h3>
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Entre no sistema.</h3>
                    </div>
                    <div class="panel-body">
                        <?php 
                        echo validation_errors('<div class="alert alert-danger">','</div>');
                        echo form_open('administrativo/usuarios/login');
                        ?>
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Usuário" name="txt-user" type="text" autofocus>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Senha" name="txt-senha" type="password" >
                                </div>
                            
                               <button class="btn btn-lg btn-danger btn-block">Entrar</button>
                            </fieldset>
                        <?php
                        echo form_close();
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
