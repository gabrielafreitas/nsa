<style type="text/css">
body {
    background-image: url(<?=base_url('assets/mdb/img/emergence.jpg')?>);
  
  
}
.img{
    background-color: white;
}
.letter{
    color: black;
}
</style>
<div id="wrapper body">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Navegação</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?=base_url('index.php/administrativo/home')?>"><img class="img" src="<?= base_url('assets/mdb/img/icons/logo2.png') ?>" width="200px"/></a>
        </div>

        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">
                    <li class="dropdown"><a class="dropdown-toggle letter" data-toggle="dropdown"><i class="fa fa-user fa-fw"></i></span> Funcionários <span class="caret"></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo base_url('index.php/administrativo/func/cadastro_func') ?>">Cadastro</a></li>
                            <li><a href="<?php echo base_url('index.php/administrativo/func/lista_func') ?>">Ver todos</a></li>
                        </ul>
                    </li>
                    <li class="dropdown"><a class="dropdown-toggle letter" data-toggle="dropdown"><i class="fa fa-tags fa-fw"></i></span> Plantões <span class="caret"></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo base_url('index.php/administrativo/plantao/cadastro_plantao') ?>">Cadastro</a></li>
                            <li><a href="<?php echo base_url('index.php/administrativo/plantao/lista_plantao') ?>">Ver todos</a></li>
                        </ul>
                    </li>
                    <li class="dropdown"><a class="dropdown-toggle letter" data-toggle="dropdown"><i class="fa fa-users fa-fw"></i></span>  Clientes <span class="caret"></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo base_url('index.php/administrativo/cliente/lista_cliente') ?>">Ver todos</a></li>
                        </ul>
                    </li>
                    <li class="dropdown"><a class="dropdown-toggle letter" data-toggle="dropdown"><i class="fa fa-tags fa-fw"></i></span> Supervisores <span class="caret"></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo base_url('index.php/administrativo/supervisor/cadastro_super') ?>">Cadastro</a></li>
                            <li><a href="<?php echo base_url('index.php/administrativo/supervisor/lista_super') ?>">Ver todos</a></li>
                        </ul>
                    </li>
                     <li class="dropdown"><a class="dropdown-toggle letter" data-toggle="dropdown"><i class="fa fa-user fa-fw"></i></span> Administradores <span class="caret"></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo base_url('index.php/administrativo/admin/cadastro_admin') ?>">Cadastro</a></li>
                            <li><a href="<?php echo base_url('index.php/administrativo/admin/lista_admin') ?>">Ver todos</a></li>
                        </ul>
                    </li>
                    <li class="dropdown"><a class="dropdown-toggle letter" data-toggle="dropdown"><i class="fa fa-tags fa-fw"></i></span> Resultado dos Negócios <span class="caret"></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo base_url('index.php/administrativo/admin/lista_jan') ?>">Janeiro</a></li>
                            <li><a href="<?php echo base_url('index.php/administrativo/admin/lista_fev') ?>">Fevereiro</a></li>
                            <li><a href="<?php echo base_url('index.php/administrativo/admin/lista_mar') ?>">Março</a></li>
                            <li><a href="<?php echo base_url('index.php/administrativo/admin/lista_abr') ?>">Abril</a></li>
                            <li><a href="<?php echo base_url('index.php/administrativo/admin/lista_mai') ?>">Maio</a></li>
                            <li><a href="<?php echo base_url('index.php/administrativo/admin/lista_jun') ?>">Junho</a></li>
                            <li><a href="<?php echo base_url('index.php/administrativo/admin/lista_jul') ?>">Julho</a></li>
                            <li><a href="<?php echo base_url('index.php/administrativo/admin/lista_ago') ?>">Agosto</a></li>
                            <li><a href="<?php echo base_url('index.php/administrativo/admin/lista_set') ?>">Setembro</a></li>
                            <li><a href="<?php echo base_url('index.php/administrativo/admin/lista_out') ?>">Outubro</a></li>
                            <li><a href="<?php echo base_url('index.php/administrativo/admin/lista_nov') ?>">Novembro</a></li>
                            <li><a href="<?php echo base_url('index.php/administrativo/admin/lista_dez') ?>">Dezembro</a></li>
                            
                        </ul>
                    </li>
                    <li>
                        <a class="letter" href="<?php echo base_url('index.php/administrativo/usuarios/logout') ?>"><i class="fa fa-sign-out fa-fw"></i> Sair do Sistema</a>
                    </li>
                </ul>
            </div>
        </div>
