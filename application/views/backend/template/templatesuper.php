<style type="text/css">
body {
    background-image: url(<?=base_url('assets/mdb/img/emergence.jpg')?>);
  
  
}
.img{
    background-color: white;
}
.letter{
    color: black;
}

</style>
<div id="wrapper">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Navegação</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?=base_url('index.php/supervisao/plantao/home')?>"><img class="img " src="<?= base_url('assets/mdb/img/icons/logo2.png') ?>" width="200px"/></a>
            
        </div>

        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">
                    <li class="dropdown"><a class="dropdown-toggle letter" data-toggle="dropdown"><i class="fa fa-tags fa-fw"></i></span> Plantões <span class="caret"></a>
                        <ul class="dropdown-menu">
                            <li><a  href="<?php echo base_url('index.php/supervisao/plantao/cadastro_plantao') ?>">Cadastro</a></li>
                            <li><a  href="<?php echo base_url('index.php/supervisao/plantao/lista_plantao') ?>">Ver todos</a></li>
                        </ul>
                    </li>
                    <li>
                        <a class="letter" href="<?php echo base_url('index.php/supervisao/supervisor/logout') ?>"><i class="fa fa-sign-out fa-fw"></i> Sair do Sistema</a>
                    </li>
                </ul>

            </div>
        </div>
