<?php
include_once APPPATH.'libraries/Funcionario.php';

class ModelFunc extends CI_Model{
    
    public function gera_tabela_func(){
        $html = ''; 

        $funcionario = new Funcionario();
        $data = $funcionario->lista_funcionario();

        foreach ($data as $funcionario){            
            $html .= '<tr>';
            $html .= '<td>'.$funcionario['id'].'</td>';
            $html .= '<td>'.$funcionario['nome'].'</td>';
            $html .= '<td>'.$funcionario['sobrenome'].'</td>';
            $html .= '<td>'.$funcionario['dt_nasc'].'</td>';
            $html .= '<td>'.$funcionario['rg'].'</td>';
            $html .= '<td>'.$funcionario['email'].'</td>';
            $html .= '<td>'.$funcionario['telefone'].'</td>';
            $html .= '<td>'.$funcionario['endereco'].'</td>';
            $html .= '<td>'.$funcionario['area'].'</td>';
            $html .= '<td>'.$this->action_buttons($funcionario['id']).'</td>';
           
            $html .= '</tr>';
        }
        return $html;
    }
    private function action_buttons($id){
        $html = '<td><a href="'.base_url('index.php/administrativo/func/editar_func/'.$id).'">';
        $html .= '<i title="Editar" style="font-size:20px" class="fa fa-edit fa-fw"></i></a></td>';
        $html .= '<td><a href="'.base_url('index.php/administrativo/func/deletar_func/'.$id).'">';
        $html .= '<i title="Deletar"  style="font-size:20px" class="fa fa-trash red-text mr-2"></i></a></td>';
        return $html;

    }


   



    public function salva_func(){
        if(sizeof($_POST) == 0) return;
        

        
        $this->form_validation->set_rules('nome','Nome','trim|required|min_length[2]|max_length[20]');
        $this->form_validation->set_rules('sobrenome','Sobrenome','trim|required|min_length[3]|max_length[60]');
        $this->form_validation->set_rules('rg','RG','trim|required|max_length[9]');
        $this->form_validation->set_rules('email','E-Mail','trim|required|min_length[8]|max_length[50]');
        $this->form_validation->set_rules('telefone','Telefone','trim|required|max_length[11]');
        $this->form_validation->set_rules('endereco','Endereço','trim|required|min_length[10]|max_length[100]');
        $this->form_validation->set_rules('area','Área');

        
        if( $this->form_validation->run()){
            
        
        $data = $this->input->post();
        $funcionario = new Funcionario();
        $funcionario->cria_func($data);
        redirect('administrativo/func/lista_func');
        } 
    }

    public function edita_func($id){

        if(sizeof($_POST) == 0) return;
       
        $data = $this->input->post();
        $funcionario = new Funcionario();
        $funcionario->edita_func($data, $id);
        redirect('administrativo/func/lista_func');

    }
    public function read_func($id){
        $funcionario = new Funcionario();
        return $funcionario->user_data_func($id);
    }

   

    public function deletar_func($id){
        $funcionario = new Funcionario();
        return $funcionario->delete_func($id);
    }

    
   
}