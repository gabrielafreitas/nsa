<?php
include_once APPPATH.'libraries/Plantoes.php';
//include_once APPPATH.'libraries/Customer.php';

class ModelFunc_Plantao extends CI_Model{
    
    public function gera_tabela(){
        $html = ''; $num = 1;

        $plantoes = new plantoes();
        $data = $plantoes->lista_plantao();

        foreach ($data as $plantao){            
            $html .= '<tr>';
            $html .= '<td>'.$plantao['id'].'</td>';
            $html .= '<td>'.$plantao['local'].'</td>';
            $html .= '<td>'.$plantao['data'].'</td>';
            $html .= '<td>'.$plantao['horario'].'</td>';
            $html .= '<td>'.$plantao['descricao'].'</td>';
                      $html .= '</tr>';
        }
        return $html;
    }


    public function gera_card(){
        $html = ''; $num = 1;
        $plantoes = new Plantoes();
        $data = $plantoes->lista_plantao();
       
         foreach ($data as $plantao){
            $html .= '<div class="container">';
            $html .= '<div class="row">';
            $html .= '<div class="col-md-5 mx-auto">';
            $html .= '<div class="card">';               
            $html .= '<div class="card-body text-center">';
            $html .= '<h1 class="card-title red">Plantão Nº '.$plantao['id'].'</h1>';
            $html .= '<h3 class="card-title">Dia: '.$plantao['data'].'</h3>'; 
            $html .= '<h5 class="card-title">Local: '.$plantao['local'].'</h5>';  
            $html .= '<h5 class="card-title">Horário: '.$plantao['horario'].'</h5>';      
            $html .= '<h4 class="card-title">Info: '.$plantao['descricao'].'</h4>'; 
            $html .= '</div>';
            $html .= '</div>';
            $html .= '</div>';
            $html .= '</div>';
            $html .= '</div>';
        }  
        return $html;   
    }



    public function read_plantao($id){
        $plantoes = new plantoes();
        return $plantoes->user_data($id);
    }
}