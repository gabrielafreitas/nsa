<?php
include_once APPPATH.'libraries/Supervisores.php';
include_once APPPATH.'libraries/Administrador.php';
include_once APPPATH.'libraries/Mes.php';

class ModelVisao extends CI_Model{
    
    public function gera_tabela(){
        $html = ''; 

        $supervisao = new supervisores();
        $data = $supervisao->lista_super();

        foreach ($data as $supervisao){            
            $html .= '<tr>';
            $html .= '<td>'.$supervisao['id'].'</td>';
            $html .= '<td>'.$supervisao['user'].'</td>';
            $html .= '<td>'.$supervisao['dt_nasc'].'</td>';
            $html .= '<td>'.$supervisao['rg'].'</td>';
            $html .= '<td>'.$supervisao['email'].'</td>';
            $html .= '<td>'.$supervisao['telefone'].'</td>';
            $html .= '<td>'.$this->action_buttons($supervisao['id']).'</td>';
           
            $html .= '</tr>';
        }
        return $html;
    }
    private function action_buttons($id){
        $html = '<td><a href="'.base_url('index.php/administrativo/supervisor/edita_super/'.$id).'">';
        $html .= '<i title="Editar" style="font-size:20px" class="fa fa-edit fa-fw"></i></a></td>';
        $html .= '<td><a href="'.base_url('index.php/administrativo/supervisor/delete_super/'.$id).'">';
        $html .= '<i title="Deletar"  style="font-size:20px" class="fa fa-trash red-text mr-2"></i></a></td>';
        return $html;

    }
        
    public function salva_super(){
        if(sizeof($_POST) == 0) return;
        
        $this->form_validation->set_rules('user','Nome','trim|required|min_length[2]|max_length[20]');
        $this->form_validation->set_rules('dt_nasc','Data de Nascimento','trim|required');
        $this->form_validation->set_rules('rg','RG','trim|required|max_length[9]');  
        $this->form_validation->set_rules('email','E-Mail','trim|required|min_length[8]|max_length[50]');
        $this->form_validation->set_rules('telefone','Telefone','trim|required|max_length[11]');
        $this->form_validation->set_rules('senha','Senha','trim|required|min_length[5]|max_length[11]|md5');
    
        if( $this->form_validation->run()){
            
        $data = $this->input->post();
        $supervisao = new Supervisores();
        $supervisao->cria_super($data);
        redirect('administrativo/supervisor/lista_super');
        }

    }

    public function edita_super($id){

        if(sizeof($_POST) == 0) return;
       
        $data = $this->input->post();
        $supervisao = new Supervisores();
        $supervisao->edita_super($data, $id);
        redirect('administrativo/supervisor/lista_super');

    }
    public function read_super($id){
        $supervisao = new Supervisores();
        return $supervisao->user_data_super($id);
    }

   

    public function delete_super($id){
        $supervisao = new Supervisores();
        return $supervisao->delete_super($id);
    }

    
    
    public function gera_tabela_admin(){
        $html = ''; 

        $administrador = new Administrador();
        $data = $administrador->lista_admin();

        foreach ($data as $administrador){            
            $html .= '<tr>';
            $html .= '<td>'.$administrador['nome'].'</td>';
            $html .= '<td>'.$administrador['email'].'</td>';
            $html .= '<td>'.$administrador['user'].'</td>';
            $html .= '<td>'.$this->action_buttons_admin($administrador['id']).'</td>';
           
            $html .= '</tr>';
        }
        return $html;
    }
    private function action_buttons_admin($id){
        $html = '<td><a href="'.base_url('index.php/administrativo/admin/edita_admin/'.$id).'">';
        $html .= '<i title="Editar" style="font-size:20px" class="fa fa-edit fa-fw"></i></a></td>';
        $html .= '<td><a href="'.base_url('index.php/administrativo/admin/delete_admin/'.$id).'">';
        $html .= '<i title="Deletar"  style="font-size:20px" class="fa fa-trash red-text mr-2"></i></a></td>';
        return $html;

    }
        
      public function salva_admin(){
        if(sizeof($_POST) == 0) return;
          
        $this->form_validation->set_rules('nome','Nome','trim|required|min_length[2]|max_length[20]'); 
        $this->form_validation->set_rules('email','E-Mail','trim|required|min_length[8]|max_length[50]');
        $this->form_validation->set_rules('user','User','trim|required|min_length[5]|max_length[11]');
        $this->form_validation->set_rules('senha','Senha','trim|required|min_length[5]|max_length[11]|md5');
    
        
        if( $this->form_validation->run()){
            
        
        $data = $this->input->post();
        $administrador = new Administrador();
        $administrador ->cria_admin($data);
        redirect('administrativo/admin/lista_admin');
        }
       
    }

    public function edita_admin($id){

        if(sizeof($_POST) == 0) return;
       
        $data = $this->input->post();
        $administrador = new Administrador();
        $administrador->edita_admin($data, $id);
        redirect('administrativo/admin/lista_admin');

    }
    public function read_admin($id){
        $administrador = new Administrador();
        return $administrador->user_data_admin($id);
    }

    public function delete_admin($id){
        $administrador = new Administrador();
        return $administrador->delete_admin($id);
    }
    
    public function gera_mes_jan(){
        $html = ''; 

        $mes = new mes();
        $data = $mes->lista_mes_jan();

        foreach ($data as $mes){            
            $html .= '<tr>';
            $html .= '<td>'.$mes['COUNT(data)'].'</td>'; 
            $html .= '<td> Janeiro </td>';           
            $html .= '</tr>';
        }
        return $html;
    }

    public function gera_mes_fev(){
        $html = ''; 

        $mes = new mes();
        $data = $mes->lista_mes_fev();

        foreach ($data as $mes){            
            $html .= '<tr>';
            $html .= '<td>'.$mes['COUNT(data)'].'</td>'; 
            $html .= '<td> Fevereiro </td>';           
            $html .= '</tr>';
        }
        return $html;
    }
    public function gera_mes_mar(){
        $html = ''; 

        $mes = new mes();
        $data = $mes->lista_mes_mar();

        foreach ($data as $mes){            
            $html .= '<tr>';
            $html .= '<td>'.$mes['COUNT(data)'].'</td>'; 
            $html .= '<td> Março </td>';           
            $html .= '</tr>';
        }
        return $html;
    }
    public function gera_mes_abr(){
        $html = ''; 

        $mes = new mes();
        $data = $mes->lista_mes_abr();

        foreach ($data as $mes){            
            $html .= '<tr>';
            $html .= '<td>'.$mes['COUNT(data)'].'</td>'; 
            $html .= '<td> Abril </td>';           
            $html .= '</tr>';
        }
        return $html;
    }
    public function gera_mes_mai(){
        $html = ''; 

        $mes = new mes();
        $data = $mes->lista_mes_mai();

        foreach ($data as $mes){            
            $html .= '<tr>';
            $html .= '<td>'.$mes['COUNT(data)'].'</td>'; 
            $html .= '<td> Maio </td>';           
            $html .= '</tr>';
        }
        return $html;
    }

    public function gera_mes_jun(){
        $html = ''; 

        $mes = new mes();
        $data = $mes->lista_mes_jun();

        foreach ($data as $mes){            
            $html .= '<tr>';
            $html .= '<td>'.$mes['COUNT(data)'].'</td>'; 
            $html .= '<td> Junho </td>';           
            $html .= '</tr>';
        }
        return $html;
    }
    public function gera_mes_jul(){
        $html = ''; 

        $mes = new mes();
        $data = $mes->lista_mes_jul();

        foreach ($data as $mes){            
            $html .= '<tr>';
            $html .= '<td>'.$mes['COUNT(data)'].'</td>'; 
            $html .= '<td> Julho </td>';           
            $html .= '</tr>';
        }
        return $html;
    }
    public function gera_mes_ago(){
        $html = ''; 

        $mes = new mes();
        $data = $mes->lista_mes_ago();

        foreach ($data as $mes){            
            $html .= '<tr>';
            $html .= '<td>'.$mes['COUNT(data)'].'</td>'; 
            $html .= '<td> Agosto </td>';           
            $html .= '</tr>';
        }
        return $html;
    }
    public function gera_mes_set(){
        $html = ''; 

        $mes = new mes();
        $data = $mes->lista_mes_set();

        foreach ($data as $mes){            
            $html .= '<tr>';
            $html .= '<td>'.$mes['COUNT(data)'].'</td>'; 
            $html .= '<td> Setembro </td>';           
            $html .= '</tr>';
        }
        return $html;
    }
    public function gera_mes_out(){
        $html = ''; 

        $mes = new mes();
        $data = $mes->lista_mes_out();

        foreach ($data as $mes){            
            $html .= '<tr>';
            $html .= '<td>'.$mes['COUNT(data)'].'</td>'; 
            $html .= '<td> Outubro </td>';           
            $html .= '</tr>';
        }
        return $html;
    }
    public function gera_mes_nov(){
        $html = ''; 

        $mes = new mes();
        $data = $mes->lista_mes_nov();

        foreach ($data as $mes){            
            $html .= '<tr>';
            $html .= '<td>'.$mes['COUNT(data)'].'</td>'; 
            $html .= '<td> Novembro </td>';           
            $html .= '</tr>';
        }
        return $html;
    }
    public function gera_mes_dez(){
        $html = ''; 

        $mes = new mes();
        $data = $mes->lista_mes_dez();

        foreach ($data as $mes){            
            $html .= '<tr>';
            $html .= '<td>'.$mes['COUNT(data)'].'</td>'; 
            $html .= '<td> Dezembro </td>';           
            $html .= '</tr>';
        }
        return $html;
    }
   

   
}