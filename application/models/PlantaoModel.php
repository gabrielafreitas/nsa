<?php
include_once APPPATH.'libraries/Plantoes.php';
//include_once APPPATH.'libraries/Customer.php';

class PlantaoModel extends CI_Model{
    
    public function gera_tabela(){
        $html = ''; $num = 1;

        $plantoes = new plantoes();
        $data = $plantoes->lista_plantao();

        foreach ($data as $plantao){            
            $html .= '<tr>';
            $html .= '<td>'.$plantao['id'].'</td>';
            $html .= '<td>'.$plantao['local'].'</td>';
            $html .= '<td>'.$plantao['data'].'</td>';
            $html .= '<td>'.$plantao['horario'].'</td>';
            $html .= '<td>'.$plantao['descricao'].'</td>';
            $html .= $this->action_buttons($plantao['id']).'</td>';
            $html .= '</tr>';
        }
        return $html;
    }

    private function action_buttons($id){
        $html = '<td><a href="'.base_url('index.php/administrativo/plantao/editar_plantao/'.$id).'">';
        $html .= '<i title="Editar" style="font-size:20px" class="fa fa-edit fa-fw"></i></a></td>';
        $html .= '<td><a href="'.base_url('index.php/administrativo/plantao/deletar_plantao/'.$id).'">';
        $html .= '<i title="Deletar"  style="font-size:20px" class="fa fa-trash red-text mr-2"></i></a></td>';
        return $html;
    }

    public function gera_card(){
        $html = ''; 
        $plantoes = new Plantoes();
        $data = $plantoes->lista_plantao();
       
         foreach ($data as $plantao){
            
            $html .= '<div class="container">';
            $html .= '<div class="row">';
            $html .= '<div class="col-md-5 mx-auto primary-color">';
            $html .= '<div class="card">';               
            $html .= '<div class="card-body text-center">';
            $html .= '<h1 class="card-title red">Plantão Nº <a>'.$plantao['id'].'</a></h1>';
            $html .= '<h3 class="card-title"><a>Dia: '.$plantao['data'].'</a></h3>'; 
            $html .= '<h5 class="card-title"><a>Local: '.$plantao['local'].'</a></h5>';  
            $html .= '<h5 class="card-title"><a>Horário: '.$plantao['horario'].'</a></h5>';      
            $html .= '<h4 class="card-title"><a>Dados: '.$plantao['descricao'].'</a></h4>'; 
            $html .= '</div>';
            $html .= '</div>';
            $html .= '</div>';
            $html .= '</div>';
            $html .= '</div>';
            $html .= '<br>';
        }  
        return $html;   
    }

    

    public function salva_plantao(){
        if(sizeof($_POST) == 0) return;

        //definir as regras de validação
        $this->form_validation->set_rules('Local', 'Local do plantao', 'trim|require|min_length[10]max_length[50]');
        $this->form_validation->set_rules('Data', 'Data do plantao', 'trim|require');
        $this->form_validation->set_rules('Horário', 'Horário do plantao', 'trim|require');
        $this->form_validation->set_rules('Descrição', 'Descrição do plantao', 'trim|require|min_length[10]max_length[100]');
 
        //realizar a validação
        if($this->form_validation->run()){
        //passou na validação
        //executar a ação do formulário
        $data = $this->input->post();
        $plantoes = new plantoes();
        $plantoes->cria_plantao($data);
        redirect('administrativo/plantao/lista_plantao');
        }
        else {
            echo validation_errors();
        }

        $data = $this->input->post();
        $plantoes = new plantoes();
        $plantoes->cria_plantao($data);
        redirect('administrativo/plantao/lista_plantao');
    }

    public function edita_plantao($id){
        if(sizeof($_POST) == 0) return;

        $data = $this->input->post();
        $plantoes = new plantoes();
        $plantoes->edita_plantao($data, $id);
        redirect('administrativo/plantao/lista_plantao');
    }

    public function read_plantao($id){
        $plantoes = new plantoes();
        return $plantoes->user_data($id);
    }

    public function deletar_plantao($id){
        $plantoes = new plantoes();
        $plantoes->delete_plantao($id);
    }
}