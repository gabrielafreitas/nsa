<?php
include_once APPPATH . 'libraries/Client.php';

class ModelCliente extends CI_Model
{

    public function salva_cliente()
    {

        if (sizeof($_POST) == 0) return;

        $this->form_validation->set_rules('nome', 'Nome do Cliente', 'trim|required|min_length[2]|max_length[60]');
        $this->form_validation->set_rules('email', 'Email do Cliente', 'trim|required|min_length[3]|max_length[60]');
        $this->form_validation->set_rules('telefone', 'Telefone do Cliente', 'trim|required|min_length[10]|max_length[11]');
        $this->form_validation->set_rules('empresa', 'Empresa do Cliente', 'trim|required|min_length[3]|max_length[60]');
        $this->form_validation->set_rules('mensagem', 'Mensagem do Cliente', 'trim|required|min_length[10]|max_length[500]');

        if ($this->form_validation->run()) {

            $data = $this->input->post();
            $cliente = new Client();
            $cliente->cria_cliente($data);
            redirect('/');
        }
    }

    public function gera_tabela_cliente()
    {
        $html = '';

        $cliente = new Client();
        $data = $cliente->lista_cliente();

        foreach ($data as $cliente) {
            $html .= '<tr>';
            $html .= '<td>' . $cliente['id'] . '</td>';
            $html .= '<td>' . $cliente['nome'] . '</td>';
            $html .= '<td>' . $cliente['email'] . '</td>';
            $html .= '<td>' . $cliente['telefone'] . '</td>';
            $html .= '<td>' . $cliente['empresa'] . '</td>';
            $html .= '<td>' . $cliente['mensagem'] . '</td>';
            $html .= '<td>' . $this->action_buttons_cliente($cliente['id']) . '</td>';

            $html .= '</tr>';
        }
        return $html;
    }
    private function action_buttons_cliente($id)
    {
        $html = '<td><a href="'.base_url('index.php/administrativo/cliente/deletar_cliente/'.$id).'">';
        $html .= '<i title="Deletar"  style="font-size:20px" class="fa fa-trash red-text mr-2"></i></a></td>';
        return $html;
    }

    public function read_cliente($id)
    {
        $cliente = new Client();
        return $cliente->user_data_cliente($id);
    }

    public function deletar_cliente($id)
    {
        $cliente = new Client();
        return $cliente->delete_cliente($id);
    }
}
