<?php
include_once APPPATH.'libraries/util/CI_Object.php';

class Funcionario extends CI_Object{

   

    public function lista_funcionario(){
        
        $rs = $this->db->get_where('funcionario'); 

        $result = $rs->result_array(); 
        
        return $result;
    }

    public function cria_func($data){
        $this->db->insert('funcionario', $data);

    }
    public function user_data_func($id){
        $cond = array('id' => $id);
        $rs = $this->db->get_where('funcionario', $cond);
        return $rs->row_array();
    }
    public function edita_func($data, $id){
        $this->db->update('funcionario', $data, "id = $id");

    }
    public function delete_func($id){
        $cond = array('id' => $id);
        $this->db->delete('funcionario', $cond);
    }

}