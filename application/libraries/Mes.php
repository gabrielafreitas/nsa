<?php
include_once APPPATH.'libraries/util/CI_Object.php';

class Mes extends CI_Object{

    public function lista_mes_jan(){
        $sql = 'SELECT COUNT(data) FROM plantao WHERE MONTH(data) = 01';
        $rs = $this->db->query($sql); 
        $result = $rs->result_array(); 
        return $result;
      
    }
    
    public function lista_mes_fev(){
        $sql = 'SELECT COUNT(data) FROM plantao WHERE MONTH(data) = 02';
        $rs = $this->db->query($sql); 
        $result = $rs->result_array(); 
        return $result;
      
    }
    
    public function lista_mes_mar(){
        $sql = 'SELECT COUNT(data) FROM plantao WHERE MONTH(data) = 03';
        $rs = $this->db->query($sql); 
        $result = $rs->result_array(); 
        return $result;
      
    }
    
    public function lista_mes_abr(){
        $sql = 'SELECT COUNT(data) FROM plantao WHERE MONTH(data) = 04';
        $rs = $this->db->query($sql); 
        $result = $rs->result_array(); 
        return $result;
      
    }
    
    public function lista_mes_mai(){
        $sql = 'SELECT COUNT(data) FROM plantao WHERE MONTH(data) = 05';
        $rs = $this->db->query($sql); 
        $result = $rs->result_array(); 
        return $result;
      
    }
    
    public function lista_mes_jun(){
        $sql = 'SELECT COUNT(data) FROM plantao WHERE MONTH(data) = 06';
        $rs = $this->db->query($sql); 
        $result = $rs->result_array(); 
        return $result;
      
    }

    
    public function lista_mes_jul(){
        $sql = 'SELECT COUNT(data) FROM plantao WHERE MONTH(data) = 07';
        $rs = $this->db->query($sql); 
        $result = $rs->result_array(); 
        return $result;
      
    }
    
    public function lista_mes_ago(){
        $sql = 'SELECT COUNT(data) FROM plantao WHERE MONTH(data) = 08';
        $rs = $this->db->query($sql); 
        $result = $rs->result_array(); 
        return $result;
      
    }
    
    public function lista_mes_set(){
        $sql = 'SELECT COUNT(data) FROM plantao WHERE MONTH(data) = 09';
        $rs = $this->db->query($sql); 
        $result = $rs->result_array(); 
        return $result;
      
    }
    
    public function lista_mes_out(){
        $sql = 'SELECT COUNT(data) FROM plantao WHERE MONTH(data) = 10';
        $rs = $this->db->query($sql); 
        $result = $rs->result_array(); 
        return $result;
      
    }
    
    public function lista_mes_nov(){
        $sql = 'SELECT COUNT(data) FROM plantao WHERE MONTH(data) = 11';
        $rs = $this->db->query($sql); 
        $result = $rs->result_array(); 
        return $result;
      
    }
    
    public function lista_mes_dez(){
        $sql = 'SELECT COUNT(data) FROM plantao WHERE MONTH(data) = 12';
        $rs = $this->db->query($sql); 
        $result = $rs->result_array(); 
        return $result;
      
    }

    

}



