<?php
include_once APPPATH.'libraries/util/CI_Object.php';

class Administrador extends CI_Object{

    public function lista_admin(){
        $rs = $this->db->get('usuario');

        $result = $rs->result_array(); 
        
        return $result;
    }
      public function cria_admin($data){
        $this->db->insert('usuario', $data);

    }
    public function user_data_admin($id){
        $cond = array('id' => $id);
        $rs = $this->db->get_where('usuario', $cond);
        return $rs->row_array();
    }
    public function edita_admin($data, $id){
        $this->db->update('usuario', $data, "id = $id");

    }
    public function delete_admin($id){
        $cond = array('id' => $id);
        $this->db->delete('usuario', $cond);
    }

}





