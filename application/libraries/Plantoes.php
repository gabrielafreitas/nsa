<?php
include_once APPPATH.'libraries/util/CI_Object.php';

class Plantoes extends CI_Object{

   

    public function lista_plantao(){
        $sql = 'SELECT * FROM plantao ORDER BY id DESC';
        $rs = $this->db->query($sql); 
        $result = $rs->result_array(); 
        return $result;
    }
    
    public function cria_plantao($data){
        $this->db->insert('plantao', $data);
    }
    public function user_data($id){
        $cond = array('id' => $id);
        $rs = $this->db->get_where('plantao', $cond);
        return $rs->row_array();
    }
    public function edita_plantao($data, $id){
        $this->db->update('plantao', $data, "id = $id");
    }

    public function delete_plantao($id){
        $cond = array('id' => $id);
        $this->db->delete('plantao', $cond);
    }
}