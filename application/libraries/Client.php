<?php
include_once APPPATH.'libraries/util/CI_Object.php';

class Client extends CI_Object{

    public function cria_cliente($data){
        $this->db->insert('cliente', $data);

    }

    public function lista_cliente(){
        
        $sql = 'SELECT * FROM cliente ORDER BY id DESC';
        $rs = $this->db->query($sql); 
        $result = $rs->result_array();        
        return $result;
    }

    public function user_data_cliente($id){
        $cond = array('id' => $id);
        $rs = $this->db->get_where('cliente', $cond);
        return $rs->row_array();
    }
    public function edita_cliente($data, $id){
        $this->db->update('cliente', $data, "id = $id");

    }

    public function delete_cliente($id){
        $cond = array('id' => $id);
        $this->db->delete('cliente', $cond);
    }

}    
   