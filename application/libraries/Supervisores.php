<?php
include_once APPPATH.'libraries/util/CI_Object.php';

class Supervisores extends CI_Object{

    public function lista_super(){
        
        $rs = $this->db->get_where('supervisao'); 

        $result = $rs->result_array(); 
        
        return $result;
    }

    public function cria_super($data){
        $this->db->insert('supervisao', $data);

    }
    public function user_data_super($id){
        $cond = array('id' => $id);
        $rs = $this->db->get_where('supervisao', $cond);
        return $rs->row_array();
    }
    public function edita_super($data, $id){
        $this->db->update('supervisao', $data, "id = $id");

    }
    public function delete_super($id){
        $cond = array('id' => $id);
        $this->db->delete('supervisao', $cond);
    }

}