-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 12-Dez-2019 às 22:12
-- Versão do servidor: 10.3.16-MariaDB
-- versão do PHP: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `nsa`
--
create database nsa;
use nsa;
-- --------------------------------------------------------
--
-- Estrutura da tabela `ci_session`
--

CREATE TABLE `ci_session` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `ci_session`
--

INSERT INTO `ci_session` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('0dt0od7o7uhrf6ljo7335ndl65o6r3q7', '::1', 1575339040, 0x5f5f63695f6c6173745f726567656e65726174657c693a313537353333383836383b757365726c6f6761646f7c4f3a383a22737464436c617373223a373a7b733a323a226964223b733a313a2232223b733a343a2275736572223b733a353a227375706572223b733a323a227267223b733a393a22353835363435363235223b733a373a2264745f6e617363223b733a31303a22323030322d30362d3132223b733a353a22656d61696c223b733a32313a2273757065727669736f724073757065727669736f72223b733a383a2274656c65666f6e65223b733a31303a2231313935323432393231223b733a353a2273656e6861223b733a33323a223039333438633230613031396265303331383338376330386466376137383364223b7d6c6f6761646f7c623a313b66756e636c6f6761646f7c4f3a383a22737464436c617373223a393a7b733a323a226964223b733a313a2235223b733a343a226e6f6d65223b733a383a224761627269656c61223b733a393a22736f6272656e6f6d65223b733a373a2246726569746173223b733a373a2264745f6e617363223b733a31303a22323030322d30362d3137223b733a323a227267223b733a393a22353739393936383135223b733a353a22656d61696c223b733a32343a226761627279656c6c616d6573736940676d61696c2e636f6d223b733a383a2274656c65666f6e65223b733a31303a2231313935323432393231223b733a383a22656e64657265636f223b733a32303a2272756120646f732067656f6772c3a1666f733333223b733a343a2261726561223b733a31303a22736f636f727269737461223b7d73757065726c6f6761646f7c4e3b757365726c6f6761646f327c4f3a383a22737464436c617373223a353a7b733a323a226964223b733a313a2231223b733a343a226e6f6d65223b733a32353a224e534120456d657267c3aa6e63696173204dc3a96469636173223b733a353a22656d61696c223b733a32353a226e73616d62756c616e6369617340686f746d61696c2e636f6d223b733a343a2275736572223b733a373a226e7361646d696e223b733a353a2273656e6861223b733a33323a223931663531363763333463343030373538313135633261363832366563326533223b7d);
-- --------------------------------------------------------

--
-- Estrutura da tabela `cliente`
--

CREATE TABLE `cliente` (
  `id` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `email` varchar(60) NOT NULL,
  `telefone` varchar(15) NOT NULL,
  `empresa` varchar(100) NOT NULL,
  `mensagem` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `cliente`
--

INSERT INTO `cliente` (`id`, `nome`, `email`, `telefone`, `empresa`, `mensagem`) VALUES
(7, 'Camila Loures', 'cummins@camilaloures.com', '11952429211', 'Cummins LTDA', 'Oi, haverá uma festa na minha empresa, preciso de uma cobertura médica!'),
(8, 'Camila Loures', 'cummins@camilaloures.com', '11952429211', 'Cummins LTDA', 'Oi, haverá uma festa na minha empresa, preciso de uma cobertura médica!');

-- --------------------------------------------------------

--
-- Estrutura da tabela `funcionario`
--

CREATE TABLE `funcionario` (
  `id` int(11) NOT NULL,
  `nome` varchar(25) NOT NULL,
  `sobrenome` varchar(35) NOT NULL,
  `dt_nasc` date NOT NULL,
  `rg` varchar(10) NOT NULL,
  `email` varchar(50) NOT NULL,
  `telefone` int(15) NOT NULL,
  `endereco` varchar(100) NOT NULL,
  `area` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `funcionario`
--

INSERT INTO `funcionario` (`id`, `nome`, `sobrenome`, `dt_nasc`, `rg`, `email`, `telefone`, `endereco`, `area`) VALUES
(7, 'Camila', 'Loures', '1991-02-12', '579996815', 'cami@cami.com', 2147483647, 'rua da camila,299', 'Médico(a)'),
(8, 'funcionario', 'func', '2019-12-09', '123456789', 'func@func.com', 1234567891, 'rua do funcionario', 'Paramédico(a)');

-- --------------------------------------------------------

--
-- Estrutura da tabela `plantao`
--

CREATE TABLE `plantao` (
  `id` int(11) NOT NULL,
  `local` varchar(100) NOT NULL,
  `data` date NOT NULL,
  `horario` time NOT NULL DEFAULT current_timestamp(),
  `descricao` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `plantao`
--

INSERT INTO `plantao` (`id`, `local`, `data`, `horario`, `descricao`) VALUES
(1, 'Rua do teste do plantão, 36', '2019-02-20', '15:30:00', 'testeteste'),
(7, 'Local para teste', '2019-02-12', '12:20:00', 'isso é um teste'),
(8, 'Local para teste', '2019-03-02', '20:20:00', 'Então, isso é só um teste ');

-- --------------------------------------------------------

--
-- Estrutura da tabela `supervisao`
--

CREATE TABLE `supervisao` (
  `id` int(11) NOT NULL,
  `user` varchar(200) NOT NULL,
  `rg` varchar(9) NOT NULL,
  `dt_nasc` date NOT NULL,
  `email` varchar(50) NOT NULL,
  `telefone` int(15) NOT NULL,
  `senha` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `supervisao`
--

INSERT INTO `supervisao` (`id`, `user`, `rg`, `dt_nasc`, `email`, `telefone`, `senha`) VALUES
(2, 'super', '585645625', '2002-06-12', 'supervisor@supervisor', 1195242921, '09348c20a019be0318387c08df7a783d');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `nome` varchar(200) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `user` varchar(50) DEFAULT NULL,
  `senha` longtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`id`, `nome`, `email`, `user`, `senha`) VALUES
(1, 'NSA Emergências Médicas', 'nsambulancias@hotmail.com', 'nsadmin', '91f5167c34c400758115c2a6826ec2e3');

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `ci_session`
--
ALTER TABLE `ci_session`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Índices para tabela `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `funcionario`
--
ALTER TABLE `funcionario`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `plantao`
--
ALTER TABLE `plantao`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `supervisao`
--
ALTER TABLE `supervisao`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `cliente`
--
ALTER TABLE `cliente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de tabela `funcionario`
--
ALTER TABLE `funcionario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de tabela `plantao`
--
ALTER TABLE `plantao`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de tabela `supervisao`
--
ALTER TABLE `supervisao`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de tabela `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
